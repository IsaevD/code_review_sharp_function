/// <summary>
/// ����� �������
/// </summary>
class Person
{
    public Guid UIN;
    public int age { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }

    // Isaev_DM: ������ ������ ����

    // Isaev_DM: ���������� ������ �� ���������� ������ - ��������� ��������� ������� �� ��������� ������� age, firstName, secondName
    // Isaev_DM: ���������� ������ �� ���������� ������ - ��������� ��������� ������� �� ��������� ������� age, firstName, secondName
    // Isaev_DM: ������������ camelCase �� ���� ��������� ����������
    
    //private int Age;
    //private string firstName;
    //private string _lastName;
    //public int PersonAge { get { return Age; } }
    //public string FirstName { get { return firstName; } }
    //public string LastName { get { return lastName; } }

    //�����������.

    public Person(Guid UIN, string name, DateTime birthDate)
    {
        string[] personalDate = name.Split(' ');
        this.UIN = UIN;
        this.firstName = personalDate[0];
        this.lastName = personalDate[1];
        this.age = (int)((DateTime.Now - birthDate).TotalDays / 365.0);

        // Isaev_DM: ������ ������ ����
        // Isaev_DM: ���������� ������������ ������������ string[] name.Split(' ')

        //this.UIN = UIN;
        //firstName = name.Split(' ')[0];
        //lastName = name.Split(' ')[1];
        //Age = (int)((DateTime.Now - birthDate).TotalDays / 365.0);
    }

}

class Program
{
    private List<Person> Persons;
    /// <summary>
    /// ���������� ������ ������ ��� �����������.
    /// </summary>
    /// <param name="filteredPersons">������ ������ ��� ����������</param>
    /// <param name="filterString">������ �� ���������� ����������</param>
    /// <returns>������ ��������������� ������.</returns>
    /// <remarks>������ ������ ���� ����� � ����: ��������1=��������1;��������2=��������2;..., ��� �������� - �������� ������� �� �������� ������������ ����������, �������� - �������� �������.</remarks>
    public List<Person> getPersonsForDisplay(List<Person> filteredPersons, string filterString)
    {

        if (filterString != null)
        {
            string[] filters = filterString.Split(';');
            string filterName;
            string filterValue;

            foreach (string filter in filters)
            {
                filterName = filter.Split('=')[0];
                filterValue = filter.Split('=')[1];
                switch (filterName)
                {
                    case "PersonAge":
                        filteredPersons = filteredPersons.Where(x => x.age == Convert.ToInt32(filterValue)).ToList();
                        break;
                    case "FirstName":
                        filteredPersons = filteredPersons.Where(x => x.firstName == filterValue).ToList();
                        break;
                    case "LastName":
                        filteredPersons = filteredPersons.Where(x => x.lastName == filterValue).ToList();
                        break;
                }
            }
        }
        return filteredPersons;

        // Isaev_DM: ������ ������ ����

        /*
         * Isaev_DM: ������� ������������ ���������������� �� ����������� (��. ���������� ������ ����)
        if (filter == null)
            return Persons;
        
        string[] filters = filter.Split(';');
        
         * Isaev_DM: � ������� � �������� ��������� ���������� ���������� ��� ��� ������ ������, ������� ���������� �����������
        List<Person> filteredPersons = new List<Person>(Persons.Count);
       
         * Isaev_DM: ���������� ���� - � ����������� ����� �������� � ������� ������ Where
        foreach (Person person in Persons)
        {
            foreach (string Filter in filters)
            {
                switch (Filter.Split('=')[0])
                {
         * Isaev_DM: Filter.Split('=')[1] - �������� �� switch, ����� ��� ��������� - ��� ��������� �� ���������� ��������
                    case "PersonAge":
                        if (person.personAge.ToString() == Filter.Split('=')[1])
                            filteredPersons.Add(person);
                        break;
                    case "FirstName":
                        if (person.FirstName == Filter.Split('=')[1])
                            filteredPersons.Add(person);
                        break;
                    case "LastName":
                        if (person.LastName == Filter.Split('=')[1])
                            filteredPersons.Add(person);
                        break;
                }
            }
        }
        return filteredPersons;
        */
    }
}

